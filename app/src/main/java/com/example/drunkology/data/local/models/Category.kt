package com.example.drunkology.data.local.models

data class Category(
    val strCategory: String
)
