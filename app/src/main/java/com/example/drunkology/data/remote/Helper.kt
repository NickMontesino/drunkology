package com.example.drunkology.data.remote

import com.example.drunkology.data.remote.api.DrinksAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object Helper {

    private val baseURL = "https://www.thecocktaildb.com"

    private fun getRetrofitInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .build()
    }

    fun getAPI(): DrinksAPI = getRetrofitInstance().create()

}