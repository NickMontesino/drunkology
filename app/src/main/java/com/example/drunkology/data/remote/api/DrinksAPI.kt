package com.example.drunkology.data.remote.api

import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.data.remote.response.CategoryListResponse
import com.example.drunkology.data.remote.response.DrinkListResponse
import com.example.drunkology.data.remote.response.DrinkResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinksAPI {

    companion object {

        private const val API_PATH: String = "/api/json/v1/1/"

        private const val LIST_ENDPOINT = "${API_PATH}list.php"
        private const val FILTER_ENDPOINT = "${API_PATH}filter.php"
        private const val LOOKUP_ENDPOINT = "${API_PATH}lookup.php"

    }

    @GET(LIST_ENDPOINT)
    suspend fun getCategories(@Query("c") list: String = "list"): Response<CategoryListResponse>

    @GET(FILTER_ENDPOINT)
    suspend fun getDrinksByCategory(@Query("c") category: String): Response<DrinkListResponse>

    @GET(LOOKUP_ENDPOINT)
    suspend fun getDrinkByID(@Query("i") id: String): Response<DrinkResponse>

}