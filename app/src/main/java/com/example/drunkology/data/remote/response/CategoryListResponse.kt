package com.example.drunkology.data.remote.response

import com.example.drunkology.data.local.models.Category
import com.google.gson.annotations.SerializedName

data class CategoryListResponse(
    @SerializedName("drinks")
    val categoryList: List<Category>
)