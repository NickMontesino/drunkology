package com.example.drunkology.data.remote.response

import com.example.drunkology.data.local.models.Drink
import com.google.gson.annotations.SerializedName

data class DrinkResponse(
    @SerializedName("drinks")
    val drink: List<Drink>
)
