package com.example.drunkology.data.repository

import com.example.drunkology.data.local.models.Category
import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.data.remote.api.DrinksAPI

class Repository(
    private val api: DrinksAPI,
) {

    suspend fun fetchCategories(): List<Category> {
        return api.getCategories().body()?.categoryList!!
    }

    suspend fun fetchDrinksFromCategory(category: String): List<Drink> {
        return api.getDrinksByCategory(category).body()?.drinkList!!
    }

    suspend fun fetchDrinkByID(id: String): Drink {
        return api.getDrinkByID(id).body()?.drink?.get(0)!!
    }

}