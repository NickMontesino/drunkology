package com.example.drunkology.domain.usecases

import com.example.drunkology.data.repository.Repository
import com.example.drunkology.ui.views.categoryFragment.CategoryState

class GetCategoriesUseCase(
    private val repo: Repository,
) {
    suspend operator fun invoke(): CategoryState {
        return try {
            CategoryState(
                categories = repo.fetchCategories()
            )
        } catch (e: Exception) {
            CategoryState(
                errorMessage = e
            )
        }
    }
}