package com.example.drunkology.domain.usecases

import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.data.repository.Repository
import com.example.drunkology.ui.views.detailFragment.DetailFragment
import com.example.drunkology.ui.views.detailFragment.DetailState
import java.lang.Exception

class GetDrinkByIdUseCase(
    private val repo: Repository
) {
    suspend operator fun invoke(
        id: String
    ): DetailState {
        return try {
            DetailState(
                drink = repo.fetchDrinkByID(id)
            )
        } catch (e: Exception) {
            DetailState(
                errorMessage = e
            )
        }
    }
}