package com.example.drunkology.domain.usecases

import com.example.drunkology.data.local.models.Category
import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.data.repository.Repository
import com.example.drunkology.ui.views.drinkFragment.DrinkState
import retrofit2.Response
import java.lang.Exception

class GetDrinksFromCategoryUseCase(
    private val repo: Repository,
    private val category: String
) {
    suspend operator fun invoke(): DrinkState {
        return try {
            DrinkState(
                drinks = repo.fetchDrinksFromCategory(category)
            )
        }
        catch (e: Exception) {
            DrinkState(
                errorMessage = e
            )
        }
    }
}