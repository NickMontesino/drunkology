package com.example.drunkology.ui.viewmodels

import androidx.lifecycle.*
import com.example.drunkology.data.local.models.Category
import com.example.drunkology.domain.usecases.GetCategoriesUseCase
import com.example.drunkology.ui.views.categoryFragment.CategoryState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoryViewModel(
    getCategoriesUseCase: GetCategoriesUseCase
) : ViewModel() {

    private val _categoriesState = MutableLiveData(CategoryState())
    val categoriesState: LiveData<CategoryState> get() = _categoriesState

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _categoriesState.value = getCategoriesUseCase()
        }
    }

}

class ViewModelFactoryCategory(
    private val useCase: GetCategoriesUseCase
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryViewModel(useCase) as T
    }
}