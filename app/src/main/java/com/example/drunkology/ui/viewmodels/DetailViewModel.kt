package com.example.drunkology.ui.viewmodels

import androidx.lifecycle.*
import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.domain.usecases.GetDrinkByIdUseCase
import com.example.drunkology.ui.views.detailFragment.DetailState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(
    private val getDrinkByIdUseCase:
    GetDrinkByIdUseCase,
    private val id: String
) : ViewModel() {

    private val _drinkState = MutableLiveData<DetailState?>(
        null
//        DetailState(
//            drink = Drink(
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                "",
//                ""
//            )
//        )
    )
    val drinkState: LiveData<DetailState?> get() = _drinkState

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _drinkState.value = getDrinkByIdUseCase(id)
        }
    }

}

class ViewModelFactoryDetail(
    private val useCase: GetDrinkByIdUseCase,
    private val id: String
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailViewModel(useCase, id) as T
    }
}