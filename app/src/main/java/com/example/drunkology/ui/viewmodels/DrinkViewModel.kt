package com.example.drunkology.ui.viewmodels

import androidx.lifecycle.*
import com.example.drunkology.data.local.models.Category
import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.domain.usecases.GetDrinksFromCategoryUseCase
import com.example.drunkology.ui.views.drinkFragment.DrinkState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DrinkViewModel(
    private val getDrinksFromCategoryUseCase:
    GetDrinksFromCategoryUseCase
) : ViewModel() {

    private val _drinkState = MutableLiveData(
        DrinkState(
            drinks = listOf(
                Drink(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
            )
        )
    )
    val drinkState: LiveData<DrinkState> get() = _drinkState

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _drinkState.value = getDrinksFromCategoryUseCase()
        }
    }

}

class ViewModelFactoryDrink(
    private val useCase: GetDrinksFromCategoryUseCase
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DrinkViewModel(useCase) as T
    }
}