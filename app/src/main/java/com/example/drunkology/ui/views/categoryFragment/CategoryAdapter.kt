package com.example.drunkology.ui.views.categoryFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.drunkology.data.local.models.Category
import com.example.drunkology.databinding.CategoryBinding

class CategoryAdapter(private val listener: (category: String) -> Unit) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    private lateinit var data: List<Category>

    class ViewHolder(private val binding: CategoryBinding, private val listener: (category: String) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun apply(data: Category) = with(binding) {
            categoryName.text = data.strCategory
            categoryCard.setOnClickListener {
                listener.invoke(data.strCategory)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyCategories(categories: List<Category>) {
        data = categories
    }

}