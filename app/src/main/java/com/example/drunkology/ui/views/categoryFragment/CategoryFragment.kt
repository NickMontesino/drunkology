package com.example.drunkology.ui.views.categoryFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drunkology.data.local.models.Category
import com.example.drunkology.data.remote.Helper
import com.example.drunkology.data.repository.Repository
import com.example.drunkology.databinding.FragmentCategoryBinding
import com.example.drunkology.domain.usecases.GetCategoriesUseCase
import com.example.drunkology.ui.viewmodels.CategoryViewModel
import com.example.drunkology.ui.viewmodels.ViewModelFactoryCategory

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding: FragmentCategoryBinding get() = _binding!!

    private val vm by viewModels<CategoryViewModel> {
        ViewModelFactoryCategory(
            GetCategoriesUseCase(
                Repository(
                    Helper.getAPI()
                )
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initListener(category: String) {
        val action = CategoryFragmentDirections.actionCategoryFragmentToDrinkFragment(category)
        findNavController().navigate(action)
    }

    private fun initObservers() = with(vm) {
        categoriesState.observe(viewLifecycleOwner) { viewState ->
            if (viewState?.isLoading == true) {
                binding.tvCategoryQuestion.text = "Generating Drink Categories..."
            }
            else if (viewState?.categories?.isNotEmpty() == true) {
                binding.rvCategories.layoutManager = LinearLayoutManager(requireContext())
                binding.rvCategories.adapter = CategoryAdapter(::initListener).apply {
                    applyCategories(viewState.categories)
                }
            }
            else if (viewState.errorMessage?.message?.isNotEmpty() == true) {
                Toast.makeText(
                    requireContext(),
                    viewState.errorMessage.localizedMessage,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

}