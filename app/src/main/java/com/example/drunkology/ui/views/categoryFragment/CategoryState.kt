package com.example.drunkology.ui.views.categoryFragment

import com.example.drunkology.data.local.models.Category
import java.lang.Exception

data class CategoryState(
    var isLoading: Boolean = false,
    val errorMessage: Exception? = null,
    val categories: List<Category>? = null
)
