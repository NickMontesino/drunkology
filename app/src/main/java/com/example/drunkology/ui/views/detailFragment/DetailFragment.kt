package com.example.drunkology.ui.views.detailFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.drunkology.data.remote.Helper
import com.example.drunkology.data.repository.Repository
import com.example.drunkology.databinding.FragmentDetailBinding
import com.example.drunkology.domain.usecases.GetDrinkByIdUseCase
import com.example.drunkology.ui.viewmodels.DetailViewModel
import com.example.drunkology.ui.viewmodels.ViewModelFactoryDetail
import com.example.drunkology.utils.loadImage

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!

    private val args by navArgs<DetailFragmentArgs>()

    private val vm by viewModels<DetailViewModel> {
        ViewModelFactoryDetail(
            GetDrinkByIdUseCase(
                Repository(
                    Helper.getAPI()
                )
            ),
            args.id
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(vm) {
        drinkState.observe(viewLifecycleOwner) { viewState ->
            if (viewState?.drink !== null) {

                binding.drinkDetailName.text = viewState.drink.strDrink
                binding.drinkDetailImage.loadImage(viewState.drink.strDrinkThumb)

                val ingredientsAndMeasurements = mutableListOf<String>()
                val ingredientsList = listOfNotNull(
                    viewState.drink.strIngredient1,
                    viewState.drink.strIngredient2,
                    viewState.drink.strIngredient3,
                    viewState.drink.strIngredient4,
                    viewState.drink.strIngredient5,
                    viewState.drink.strIngredient6,
                    viewState.drink.strIngredient7,
                    viewState.drink.strIngredient8,
                    viewState.drink.strIngredient9,
                    viewState.drink.strIngredient10,
                    viewState.drink.strIngredient11,
                    viewState.drink.strIngredient12,
                    viewState.drink.strIngredient13,
                    viewState.drink.strIngredient14,
                    viewState.drink.strIngredient15,
                )
                val measurementsList = listOfNotNull(
                    viewState.drink.strMeasure1,
                    viewState.drink.strMeasure2,
                    viewState.drink.strMeasure3,
                    viewState.drink.strMeasure4,
                    viewState.drink.strMeasure5,
                    viewState.drink.strMeasure6,
                    viewState.drink.strMeasure7,
                    viewState.drink.strMeasure8,
                    viewState.drink.strMeasure9,
                    viewState.drink.strMeasure10,
                    viewState.drink.strMeasure11,
                    viewState.drink.strMeasure12,
                    viewState.drink.strMeasure13,
                    viewState.drink.strMeasure14,
                    viewState.drink.strMeasure15,
                )

                for (i in ingredientsList.indices) {
                    if (i < measurementsList.size) {
                        ingredientsAndMeasurements.add(
                            i,
                            "- ${measurementsList[i]} of ${ingredientsList[i]}"
                        )
                    }
                    else {
                        ingredientsAndMeasurements.add(
                            i,
                            "- ${ingredientsList[i]}"
                        )
                    }
                }

                binding.ingredients.text =
                    ingredientsAndMeasurements.joinToString("\n")

                binding.instructions.text = viewState.drink.strInstructions

            }
            else if (viewState?.isLoading == true) {
                // TODO:
            }
            else if (viewState?.errorMessage?.localizedMessage?.isNotEmpty() == true) {
                // TODO:
            }
        }
    }

}