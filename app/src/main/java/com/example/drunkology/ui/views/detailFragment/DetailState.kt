package com.example.drunkology.ui.views.detailFragment

import com.example.drunkology.data.local.models.Drink
import java.lang.Exception

data class DetailState(
    var isLoading: Boolean = false,
    val errorMessage: Exception? = null,
    val drink: Drink? = null
)
