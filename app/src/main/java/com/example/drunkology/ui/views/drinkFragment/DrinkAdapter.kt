package com.example.drunkology.ui.views.drinkFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.drunkology.data.local.models.Drink
import com.example.drunkology.databinding.DrinkBinding
import com.example.drunkology.utils.loadImage

class DrinkAdapter(private val listener: (id: String) -> Unit) : RecyclerView.Adapter<DrinkAdapter.ViewHolder>() {

    private lateinit var data: List<Drink>

    class ViewHolder(private val binding: DrinkBinding, private val listener: (id: String) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun apply(data: Drink) = with(binding) {
            drinkPreview.loadImage(data.strDrinkThumb)
            drinkName.text = data.strDrink
            drinkCard.setOnClickListener {
                listener.invoke(data.idDrink)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DrinkBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyDrinks(drinks: List<Drink>) {
        data = drinks
    }

}