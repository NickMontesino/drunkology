package com.example.drunkology.ui.views.drinkFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drunkology.data.remote.Helper
import com.example.drunkology.data.repository.Repository
import com.example.drunkology.databinding.FragmentDrinkBinding
import com.example.drunkology.domain.usecases.GetDrinksFromCategoryUseCase
import com.example.drunkology.ui.viewmodels.DrinkViewModel
import com.example.drunkology.ui.viewmodels.ViewModelFactoryDrink

class DrinkFragment : Fragment() {

    private var _binding: FragmentDrinkBinding? = null
    private val binding: FragmentDrinkBinding get() = _binding!!

    private val args by navArgs<DrinkFragmentArgs>()
    private fun formatCategory(category: String): String {
        return category.split(' ').joinToString("_")
    }

    private val vm by viewModels<DrinkViewModel> {
        ViewModelFactoryDrink(
            GetDrinksFromCategoryUseCase(
                Repository(
                    Helper.getAPI()
                ),
                formatCategory(args.category)
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initListener(id: String) {
        val action = DrinkFragmentDirections.actionDrinkFragmentToDetailFragment(id)
        findNavController().navigate(action)
    }

    private fun initObservers() = with(vm) {
        binding.tvDrinkCategoryTitle.text = args.category
        drinkState.observe(viewLifecycleOwner) { viewState ->
            if (viewState.isLoading) {
                binding.tvDrinkCategoryTitle.text = "Generating Drinks from Category..."
            }
            else if (viewState.drinks?.isNotEmpty()!!) {
                binding.rvDrinks.layoutManager = LinearLayoutManager(requireContext())
                binding.rvDrinks.adapter = DrinkAdapter(::initListener).apply {
                    applyDrinks(viewState.drinks)
                }
            }
            else if (viewState.errorMessage?.localizedMessage?.isNotEmpty()!!) {
                Toast.makeText(
                    requireContext(),
                    viewState.errorMessage.localizedMessage,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

}