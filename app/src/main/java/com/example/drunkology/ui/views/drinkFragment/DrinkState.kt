package com.example.drunkology.ui.views.drinkFragment

import com.example.drunkology.data.local.models.Drink
import java.lang.Exception

data class DrinkState(
    var isLoading: Boolean = false,
    val errorMessage: Exception? = null,
    val drinks: List<Drink>? = null
)
